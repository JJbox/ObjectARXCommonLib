#include "StdAfx.h"
#include "SK_SelectionSetUtil.h"


CSK_SelectionSetUtil::CSK_SelectionSetUtil(void)
{
}


CSK_SelectionSetUtil::~CSK_SelectionSetUtil(void)
{
}
//获得预选集合
int CSK_SelectionSetUtil::getSelectImplied(ads_name &ss)
{
	struct resbuf *pResBufPicked;
	struct resbuf *pResBufSelected;
	int nRet= acedSSGetFirst(&pResBufPicked,&pResBufSelected);
	if (nRet==RTNORM)
	{
		ss[0]=pResBufSelected->resval.rlname[0];
		ss[1]=pResBufSelected->resval.rlname[1];
		acedSSFree(pResBufPicked->resval.rlname);
		acutRelRb(pResBufPicked);
		acutRelRb(pResBufSelected);			
	}
	return nRet;
}
int CSK_SelectionSetUtil::getSelectImplied(AcDbObjectIdArray &objIdArray)
{
	ads_name ss;	
	int nRet=getSelectImplied(ss);
	if (RTNORM != nRet)
	{
		return nRet;
	}	
	nRet = SSToObjectIdArray(objIdArray,ss);	
	return nRet;
}
//选择集转objectIdArray
int CSK_SelectionSetUtil::SSToObjectIdArray(AcDbObjectIdArray &objIdArray,ads_name &ss)
{
	Adesk::Int32 nLength=0;
	int nRet = acedSSLength(ss,&nLength);
	if (RTNORM != nRet)
	{
		return nRet;
	}
	AcDbObjectId objId=AcDbObjectId::kNull;
	ads_name ent;
	Acad::ErrorStatus es;
	for (Adesk::Int32 i=0;i<nLength;i++)
	{
		nRet= acedSSName(ss,i,ent);
		if (RTNORM == nRet)
		{
			es = acdbGetObjectId(objId,ent);
			if (Acad::eOk == es)
			{
				objIdArray.append(objId);
			}
		}
	}
	acedSSFree(ss);
	return nRet;
}
//objectIdArray转选择集
int CSK_SelectionSetUtil::objectIdArrayToSS(AcDbObjectIdArray &objIdArray,ads_name &ss)
{
	int nRet=RTERROR;
	Adesk::Int32 nLength=objIdArray.length();
	if (nLength <= 0)
	{
		return nRet;
	}
	for (Adesk::Int32 i=0;i<nLength;i++)
	{
		ads_name ent;
		Acad::ErrorStatus es=acdbGetAdsName(ent,objIdArray[i]);
		if (Acad::eOk == es)
		{
			nRet = acedSSAdd(ent,ss,ss);
			acedSSFree(ent);
		}		
	}
	return nRet;
}

