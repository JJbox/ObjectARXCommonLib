#pragma once
//自动释放选择集类
class CSK_MySS
{
public:
	ads_name mySet;
	CSK_MySS(void){acedSSAdd(NULL,NULL,mySet);}
	~CSK_MySS(void){acedSSFree(mySet);}
	//用法CSK_MySS sets;
	//acedSSGet(NULL, NULL, NULL, NULL, sets.mySet);
};
class CSK_SelectionSetUtil
{
public:
	CSK_SelectionSetUtil(void);
	~CSK_SelectionSetUtil(void);
	//获得预选集合,返回ads_name
	static int getSelectImplied(ads_name &ss);
	//获得预选集合,返回objectIdArray
	static int getSelectImplied(AcDbObjectIdArray &objIdArray);
	//选择集转objectIdArray
	static int SSToObjectIdArray(AcDbObjectIdArray &objIdArray,ads_name &ss);
	//objectIdArray转选择集
	static int objectIdArrayToSS(AcDbObjectIdArray &objIdArray,ads_name &ss);
};

