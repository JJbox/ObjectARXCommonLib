#include "StdAfx.h"
#include "SK_CreateEnt.h"


CSK_CreateEnt::CSK_CreateEnt(void)
{
}


CSK_CreateEnt::~CSK_CreateEnt(void)
{
}
Acad::ErrorStatus CSK_CreateEnt::PostCurrentSpace(AcDbObjectId &objId,AcDbEntity *pEnt,AcDbDatabase*pDb/*=acdbHostApplicationServices()->workingDatabase()*/)
{	
	Acad::ErrorStatus es=Acad::eNullObjectId;
	AcDbObjectId blkRcdId=pDb->currentSpaceId();
	if (AcDbObjectId::kNull != blkRcdId)
	{
		AcDbBlockTableRecordPointer pBlkTblRcd(blkRcdId,AcDb::kForWrite);	
		es=pBlkTblRcd.openStatus();
		if	(Acad::eOk == es)
		{
			es= pBlkTblRcd->appendAcDbEntity(objId, pEnt);
			if (Acad::eOk == es)
			{
				pEnt->close();
			}			
		}				
	}	
	if (Acad::eOk!=es)
	{
		if (pEnt!=NULL)
		{
			delete pEnt;
			pEnt=NULL;
		}
	}
	return es;
}
Acad::ErrorStatus CSK_CreateEnt::CreateLine(AcDbObjectId &objId,const AcGePoint2d &ptStart,const AcGePoint2d &ptEnd)
{
	Acad::ErrorStatus es=Acad::eOk;
	AcGePoint3d pt1(ptStart.x,ptStart.y,0.0);
	AcGePoint3d pt2(ptEnd.x,ptEnd.y,0.0);
	AcDbLine *pLine=new AcDbLine(pt1,pt2);
	if (NULL != pLine)
	{
		return Acad::eOutOfMemory;
	}
	es=PostCurrentSpace(objId,pLine);	
	if (AcDbObjectId::kNull != objId)
	{
		return es;
	}
	return es;
}

//创建圆
Acad::ErrorStatus CSK_CreateEnt::CreateCirlce(AcDbObjectId &objId,const AcGePoint3d &ptCenter,double dRadius)
{
	Acad::ErrorStatus es=Acad::eOk;
	AcDbCircle *pCircle=new AcDbCircle(ptCenter,AcGeVector3d::kZAxis,dRadius);
	if (NULL == pCircle)
	{
		return Acad::eOutOfMemory;
	}
	es=PostCurrentSpace(objId,pCircle);
	if (AcDbObjectId::kNull != objId)
	{
		return es;
	}
	return es;
}
//创建直线
AcDbObjectId CSK_CreateEnt::CreateLine(const AcGePoint3d &ptStart,const AcGePoint3d &ptEnd)
{
	AcDbObjectId objId=AcDbObjectId::kNull;
	AcDbLine *pLine=new AcDbLine(ptStart,ptEnd);
	if (NULL == pLine)
	{
		return objId;
	}
	objId =PostCurrentSpace(pLine);
	return objId;
}
AcDbObjectId CSK_CreateEnt::PostCurrentSpace(AcDbEntity *pEnt)
{
	AcDbObjectId objId=AcDbObjectId::kNull;
	AcDbBlockTable* pBlkTbl=NULL;
	acdbHostApplicationServices()->workingDatabase()->getBlockTable(pBlkTbl,AcDb::kForRead);
	AcDbBlockTableRecord *pBlkTblRcd=NULL;
	pBlkTbl->getAt(ACDB_MODEL_SPACE,pBlkTblRcd,AcDb::kForWrite);
	pBlkTbl->close();
	pBlkTblRcd->appendAcDbEntity(objId, pEnt);
	pEnt->close();
	pBlkTblRcd->close();	
	return objId;
}
