#pragma once
class CSK_CreateEnt
{
public:
	CSK_CreateEnt(void);
	~CSK_CreateEnt(void);
	static AcDbObjectId CreateLine(const AcGePoint3d &ptStart,const AcGePoint3d &ptEnd);
	static Acad::ErrorStatus CreateLine(AcDbObjectId &objId,const AcGePoint2d &ptStart,const AcGePoint2d &ptEnd);
	static AcDbObjectId PostCurrentSpace(AcDbEntity *pEnt);
	static Acad::ErrorStatus PostCurrentSpace(AcDbObjectId &objId,AcDbEntity *pEnt,AcDbDatabase *pDb=acdbHostApplicationServices()->workingDatabase());
	//����Բ
	static Acad::ErrorStatus CreateCirlce(AcDbObjectId &objId,const AcGePoint3d &ptCenter,double dRadius);
};

